import java.util.Scanner;

public class TestaProblema{

    public static void main(String[] args) {
        
        System.out.println("###### Sistema Academico #########");
        
        Scanner le = new Scanner(System.in);
        
        System.out.print ("Informe o nome do aluno(a) 1: ");
        Aluno a1 = new Aluno(le.next(), 5);
        System.out.print ("Informe a nota do aluno(a) 1: ");
        a1.setNota(le.nextDouble());
        
        if (a1.estaAprovado()){
            //System.out.println(a1.estaAprovado()); retorna false ou true
            System.out.println ("O aluno(a) " + a1.getNome() + " está aprovado!");
        } else {
            //System.out.println(a1.estaAprovado());
            System.out.println ("O aluno(a) " + a1.getNome() + " está reprovado!");
        }
        
        Aluno a2 = new Aluno();
        System.out.print ("Informe o nome do aluno(a) 2: ");
        a2.setNome(le.next());
        System.out.print ("Informe a nota do aluno(a) 2: ");
        a2.setNota(le.nextDouble());
        //a1.setNome("Zé");

        if (a2.estaAprovado()){
            //System.out.println(a2.estaAprovado()); retorna false ou true
            System.out.println ("O aluno(a) " + a2.getNome() + " está aprovado!");
        } else {
            //System.out.println(a2.estaAprovado());
            System.out.println ("O aluno(a) " + a2.getNome() + " está reprovado!");
        }
        
        //falta o aluno com a maior nota
        //o cogigo a seguir nao poderia estar no metodo main, ou seja, aqui
        // nao deve ter calculo no metodo main
        if (a1.getNota() > a2.getNota()) {
            System.out.println ("O aluno(a) " + a1.getNome() + " tem a maior nota!!");
        } else {
            System.out.println ("O aluno(a) " + a2.getNome() + " tem a maior nota!!");
        }
    }
}
