

 
public class Aluno{

    //Atributos
      private String nome;
      private double nota;
    
    //Construtor
    public Aluno(String nome, double nota) {
        this.nome = nome;
        this.nota = nota;
    }
    
    public Aluno() {
        this.nome = "";
        this.nota = 0.0;
    }
    
    //Metodos
    public boolean estaAprovado() {
        return this.nota >= 7;
        /*if (this.nota >=7) {
            return true;
        } else {
            return false;
        }*/
    }
    
    //Metodos de acesso (Encapsulamento)
    
    public void setNome(String nome) {
        if(nome != null && !nome.equals("")) {
            this.nome = nome;   
        }
    }
    
    public String getNome() {
         return this.nome;   
    }
    
    public void setNota (double nota) {
        if (this.nota <= 10 && this.nota >= 0){
            this.nota = nota;     
        }
    }
    
    public double getNota() {
        return this.nota;   
    }
    
}
